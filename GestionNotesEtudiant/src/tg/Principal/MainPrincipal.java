package tg.Principal;

import java.sql.Connection;
import java.util.Scanner;

import tg.gestionNotes.Admin;
import tg.gestionNotes.Enseignant;
import tg.gestionNotes.Etudiant;

public class MainPrincipal {


	private static Scanner scan;

	public static void main(String[] args) {
			
		String URL = "jdbc:mysql://localhost:3306/gestionnote";
		String ClassName = "con.mysql.jdbc.Driver";
		Connexion con = new Connexion(URL, "root", "", ClassName);
        Connection connexion = con.connectToDB();
        if(connexion == null) {
        	System.out.println("Connexion perdue ");
        }else {
        	System.out.println("CONNECTED");
        }
	
		int choix;
		scan = new Scanner(System.in);
		System.out.println("************************************************************************************************");
		
		System.out.println(" ************************Bienvenu(e) sur votre Gestion de notes **************************\n");
		
		System.out.println("*************************************************************************************************");
									
		System.out.println("Veuillez faire un choix d'action pour continuer");
									
		System.out.println("1-Action sur les Etudiants \n2-Action sur les Enseignants \n3-Actions sur les UE\n4-Page Etudiant \n5-Page Enseignant \n");
		
		choix = scan.nextInt();
		
		switch(choix) {
		case 1:
			Admin admin = new Admin();
			admin.ManageEntityNamed("Etudiants");
			break;
		case 2:
			Admin administrator = new Admin();
			administrator.ManageEntityNamed("Enseignants");
			break;
		case 3:
			Admin admninistrer = new Admin();
			admninistrer.ManageEntityNamed("UE");
			break;
		case 4:
			int action;
			Etudiant etudiant = new Etudiant();
		    Scanner sc = new Scanner(System.in);
			System.out.println("**************************Bienvenu(e) sur la page etudiant ***********************\n");
			System.out.println("Que voulez vous faire: \n");
			System.out.println("1-Consultation \n2-Reclamation\n");
			action = sc.nextInt();
			etudiant.actions(action);
			break;
		case 5:
			Enseignant enseignant = new Enseignant();
			enseignant.Manage("Etudiants");
			break;
			
		default:
			
			System.out.println("Aucune action choisie!");
		}
	}


}
