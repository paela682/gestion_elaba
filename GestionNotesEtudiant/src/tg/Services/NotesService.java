package tg.Services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import tg.Principal.Connexion;
import tg.gestionNotes.Notes;

public class NotesService {

	public boolean Addnote(Notes note) {
		Connexion connect = new Connexion("jdbc:mysql://localhost:3306/gestionnote", "root", "", "con.mysql.jdbc.Driver");
		Connection con = connect.connectToDB();
		if(con != null) {
			try {
				String sql = "INSERT INTO notes(Id_Etudiant, Ue_id, Note) VALUES('"+note.getEtudiants().getId()+"','"+note.getUes().getCodeUE()+"','"+note.getNote()+"')";
				Statement stat = con.createStatement();
				System.out.println("Effecutué avec succes");
				stat.executeUpdate(sql);
				return true;
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return false;
		
		
	}
	
	public List<Notes> getAll(int id) {
		Connexion connect = new Connexion("jdbc:mysql://localhost:3306/gestionnote", "root", "", "con.mysql.jdbc.Driver");
		Connection con = connect.connectToDB();
		if(con != null) {
			try {
				Statement stat = con.createStatement();
				String sql = "SELECT * FROM notes WHERE Id_Etudiant="+id;
				ResultSet data = stat.executeQuery(sql);
				while(data.next()){
					String Unite_Enseignement = data.getString("Ue_id");
					float Note_obtenue = data.getFloat("Note");
					
					System.out.println("Unite d'enseignement \t: " +Unite_Enseignement);
					System.out.println("Votre note \t:" + Note_obtenue);
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return null;
		
	}
	public List<Notes> getByCodeUE(int id, String codeUE) {
		Connexion connect = new Connexion("jdbc:mysql://localhost:3306/gestionnote", "root", "", "con.mysql.jdbc.Driver");
		Connection con = connect.connectToDB();
		if(con != null) {
			try {
				Statement stat = con.createStatement();
				String sql = "SELECT * FROM notes WHERE Id_Etudiant="+id+" and Ue_id='"+codeUE+"'";
				ResultSet data = stat.executeQuery(sql);
				while(data.next()){
					String Unite_Enseignement = data.getString("Ue_id");
					float Note_obtenue = data.getFloat("Note");
					
					System.out.println("Unite d'enseignement \t: " +Unite_Enseignement);
					System.out.println("Votre note \t:" + Note_obtenue);
				}
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		return null;
		
	}

}
