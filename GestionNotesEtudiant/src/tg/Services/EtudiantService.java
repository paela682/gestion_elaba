package tg.Services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import tg.Principal.Connexion;
import tg.gestionNotes.Etudiant;

public class EtudiantService {

	public boolean EnregistrerEtudiant(Etudiant etudiant) {
		Connexion connect = new Connexion("jdbc:mysql://localhost:3306/gestionnote", "root", "", "con.mysql.jdbc.Driver");
		Connection con = connect.connectToDB();
		if(con != null) {
			try {
				String sql = "INSERT INTO etudiants(Nom, Prenom, Sexe, Username, Password) VALUES ('"+etudiant.getNom()+"','"+etudiant.getPrenom()+"','"+etudiant.getSexe()+"','"+etudiant.getUsername()+"','"+etudiant.getPassword()+"')";
				Statement stat = con.createStatement();
				System.out.println("Ajout effectu� avec succes!!");
				stat.executeUpdate(sql);
				return true;
					
			}catch(Exception e) {
				e.printStackTrace();
			}
			
		}
		return false;	
	}
    public Etudiant getIt() {
		Connexion connect = new Connexion("jdbc:mysql://localhost:3306/gestionnote", "root", "", "con.mysql.jdbc.Driver");
		Connection con = connect.connectToDB();
			try {
				Statement stat = con.createStatement();
				String sql = "SELECT * FROM etudiants";
				ResultSet resultat = stat.executeQuery(sql);
				List<Etudiant> etudiants = new ArrayList<Etudiant>();
				Etudiant etud = null;
				while(resultat.next()) {
					etud = new Etudiant();
					etud.setId(resultat.getInt("id"));
					etud.setNom(resultat.getString("Nom"));
					etud.setPrenom(resultat.getString("Prenom"));
					etudiants.add(etud);
					System.out.println(etud);
				}			
			}catch(Exception e) {
				e.printStackTrace();
			}
			return null;
	}
    public Etudiant getById(int id) {
		Connexion connect = new Connexion("jdbc:mysql://localhost:3306/gestionnote", "root", "", "con.mysql.jdbc.Driver");
		Connection con = connect.connectToDB();
		try {
			Statement stat = con.createStatement();
			String sql = "SELECT Id, Nom, Prenom FROM etudiants WHERE Id="+id;
			ResultSet resultat = stat.executeQuery(sql);
			Etudiant etudiant = null; 
			while(resultat.next()) {
				etudiant = new Etudiant();
				etudiant.setId(resultat.getInt("Id"));
				etudiant.setNom(resultat.getString("Nom"));
				etudiant.setPrenom(resultat.getString("Prenom"));
			}
			return etudiant;
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
    	
    }
	public Etudiant verifUser(int id) {
		Connexion connect = new Connexion("jdbc:mysql://localhost:3306/gestionnote", "root", "", "con.mysql.jdbc.Driver");
		Connection con = connect.connectToDB();
		try {
			Statement stat = con.createStatement();
			String sql = "SELECT Username, Password FROM etudiants WHERE Id="+id;
			ResultSet resultat = stat.executeQuery(sql);
			Etudiant etudiant = null;
			System.out.println(resultat);
			while(resultat.next()) {
				etudiant = new Etudiant();
				etudiant.setUsername(resultat.getString("Username"));
				etudiant.setPassword(resultat.getString("Password"));
			}
            return etudiant;
			
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		return null;
    	
    }
}
