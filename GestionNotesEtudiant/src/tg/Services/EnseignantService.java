package tg.Services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import tg.Principal.Connexion;
import tg.gestionNotes.Enseignant;

public class EnseignantService {

	public boolean AddEnseignant(Enseignant enseignant) {
		Connexion con = new Connexion("jdbc:mysql://localhost:3306/gestionnote", "root", "", "con.mysql.jdbc.Driver");
		Connection connexion = con.connectToDB();
		if(connexion != null) {
			try {
				String sql = "INSERT INTO enseignants(Nom, Prenom, Sexe, Username, Password, Id_ue) VALUES('"+enseignant.getNom()+"','"+enseignant.getPrenom()+"','"+enseignant.getSexe()+"','"+enseignant.getUsername()+"','"+enseignant.getPassword()+"','"+enseignant.getUe().getCodeUE()+"')";
				Statement stat = connexion.createStatement();
				stat.executeUpdate(sql);
				System.out.println("Succesfull");
				return true;
			}catch(Exception e) {
				e.printStackTrace();
			}
		}	
		return false;
	}
	
	public Enseignant verifUser(int id) {
		Connexion connect = new Connexion("jdbc:mysql://localhost:3306/gestionnote", "root", "", "con.mysql.jdbc.Driver");
		Connection con = connect.connectToDB();
		try {
			Statement stat = con.createStatement();
			String sql = "SELECT Username, Password FROM enseignants WHERE Id="+id;
			ResultSet resultat = stat.executeQuery(sql);
			Enseignant enseignant = null;
			while(resultat.next()) {
				enseignant = new Enseignant();
				enseignant.setUsername(resultat.getString("Username"));
				enseignant.setPassword(resultat.getString("Password"));
			}
            return enseignant;
			
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		return null;
    	
    }
	public int getenseignantId(String refUE) {
		Connexion connect = new Connexion("jdbc:mysql://localhost:3306/gestionnote", "root", "", "con.mysql.jdbc.Driver");
		Connection con = connect.connectToDB();
		try {
			Statement stat = con.createStatement();
			String sql = "SELECT Id FROM enseignants WHERE Id_ue='"+refUE+"'";
			ResultSet resultat = stat.executeQuery(sql);
			while(resultat.next()) {
				int Identifiant = resultat.getInt("Id");
				return Identifiant;
			}
			
		}catch(Exception e) {
			e.printStackTrace();
			
		}
		
	return 0;
  }
	public void consulterInbox(int Id) {
		Connexion connect = new Connexion("jdbc:mysql://localhost:3306/gestionnote", "root", "", "con.mysql.jdbc.Driver");
		Connection con = connect.connectToDB();
		try {
			Statement stat = con.createStatement();
			String sql = "SELECT Contenu FROM inbox WHERE Id_enseignant="+Id;
			ResultSet resultat = stat.executeQuery(sql);
			while(resultat.next()) {
				String Message = resultat.getString("Contenu");
				System.out.println(Message);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		
	}

}
