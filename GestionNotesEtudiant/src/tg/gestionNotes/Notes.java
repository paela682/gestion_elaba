package tg.gestionNotes;

public class Notes {

	private float Note;
	private Etudiant etudiants;
	private Ue ues;
	public float getNote() {
		return Note;
	}
	public void setNote(float note) {
		Note = note;
	}
	public Etudiant getEtudiants() {
		return etudiants;
	}
	public void setEtudiants(Etudiant etudiants) {
		this.etudiants = etudiants;
	}
	public Ue getUes() {
		return ues;
	}
	public void setUes(Ue ues) {
		this.ues = ues;
	}

}
