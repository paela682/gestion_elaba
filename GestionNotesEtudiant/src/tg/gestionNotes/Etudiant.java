package tg.gestionNotes;

import java.util.Scanner;

import tg.Services.EnseignantService;
import tg.Services.EtudiantService;
import tg.Services.NotesService;
import tg.Services.ReclamationService;
import tg.Services.UeService;

public class Etudiant extends Users {

	private Scanner sc;

	public Scanner getSc() {
		return sc;
	}

	public void setSc(Scanner sc) {
		this.sc = sc;
	}
	
	public void actions(int action) {
		EtudiantService etudserv = new EtudiantService();
		NotesService Noteser = new NotesService();
		int id;
		String Username;
		String Password;
		sc = new Scanner(System.in);
		switch(action) {
		case 1:
			System.out.println("****************************Bienvenue Etudiant*****************************");
			System.out.println("************************Authentifiez vous pour continuer***********************************\n");
			System.out.println("Votre identifiant : ");
			id = sc.nextInt();
			System.out.println("Ok entrez votre nom d'utilisateur :");
			Username = sc.nextLine();
			Username = sc.nextLine();
			System.out.println("Ok indiquer votre mot de passe : ");
			Password = sc.nextLine();
			
			Etudiant etud = etudserv.verifUser(id);
			
			if(etud.getUsername().equals(Username) && etud.getPassword().equals(Password)) {
				
				System.out.println("Connected:  \t  Profil "+etud.getUsername()+"\n");
				System.out.println(" ---------Affichage des notes et les ue ----------\n");
				Noteser.getAll(id);	
			}else {
				System.out.println("Authentification failed");
			}
			
			break;
		case 2:
			String mail;
			System.out.println("---------------------- Bienvenu(e) cher Etudiant --------");
			System.out.println("------------------------ Veuillez vous authentifier pour continuer---------------------\n");
			System.out.println("Votre identifiant svp (un entier entre 0 et plus) : ");
			id = sc.nextInt();
			System.out.println("Ok renseigner votre nom d'utilisateur :");
			Username = sc.nextLine();
			Username = sc.nextLine();
			System.out.println("Ok indiquer votre mot de passe : ");
			Password = sc.nextLine();
			
			Etudiant etud1 = etudserv.verifUser(id);
			
			if((etud1.getUsername().equals(Username) && etud1.getPassword().equals(Password))) {
				System.out.println("Connected:  \t  Profil "+etud1.getUsername()+"\n");
				System.out.println(" --------- Formulaire de Reclamation  ----------\n");
				Reclamation rec = new Reclamation();
				UeService uesr = new UeService();
				EnseignantService enserv = new EnseignantService();
				Etudiant etu = etudserv.getById(id);
				rec.setEtudiants(etu);
				
				System.out.println("Indiquer  l'unite d'enseignement dans lequel vous souhaiter reclamer: ");
				String Libelle = sc.nextLine();
				Ue u = uesr.getByNom(Libelle);
				rec.setUe(u);
				
				
				System.out.println(" veuillez indiquer la note que vous contester");
				float note = sc.nextFloat();
				rec.setNotevoulue(note);
				
				mail = "Bonjour Monsieur Je viens vers vous faire une reclamation ,Vous avez attribuer une note dont je suis pas sur [Nom :"+etu.getNom()+", Prenom:"+etu.getPrenom()+",Note:"+note+"]";
				
				ReclamationService rc = new ReclamationService();
				rc.Addreclamation(rec,enserv.getenseignantId(u.getCodeUE()),mail);

			}
			
			break;
		default:
			System.out.println("Aucune action encours");
		}
	}


}
