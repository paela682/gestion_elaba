package tg.gestionNotes;

public class Ue {

	private String CodeUE;
	private String Libelle;
	private int Credits;
	
	


	public String getCodeUE() {
		return CodeUE;
	}


	public void setCodeUE(String codeUE) {
		CodeUE = codeUE;
	}


	public String getLibelle() {
		return Libelle;
	}


	public void setLibelle(String libelle) {
		Libelle = libelle;
	}


	public int getCredits() {
		return Credits;
	}


	public void setCredits(int credits) {
		Credits = credits;
	}
	
	public String toString(){
		return "Ue [CodeUe="+ CodeUE +", Libelle="+Libelle+", Credits="+ Credits+"]";				
    }


	
	

}
