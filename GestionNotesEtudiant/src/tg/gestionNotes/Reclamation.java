package tg.gestionNotes;

public class Reclamation {
	private Etudiant etudiants;
	private float Notevoulue;
	private Ue ue;
	public Etudiant getEtudiants() {
		return etudiants;
	}
	public void setEtudiants(Etudiant etudiants) {
		this.etudiants = etudiants;
	}
	public float getNotevoulue() {
		return Notevoulue;
	}
	public void setNotevoulue(float notevoulue) {
		Notevoulue = notevoulue;
	}
	public Ue getUe() {
		return ue;
	}
	public void setUe(Ue ue) {
		this.ue = ue;
	}
	

}
