package tg.gestionNotes;

import java.util.Scanner;

import tg.Services.EnseignantService;
import tg.Services.EtudiantService;
import tg.Services.UeService;
import tg.Services.NotesService;

public class Enseignant extends Users {
	private Ue ue;

	public Ue getUe() {
		return ue;
	}

	public void setUe(Ue ue) {
		this.ue = ue;
	}
	Scanner scan = new Scanner(System.in);
	public void Manage(String entity) {
		EnseignantService enser = new EnseignantService();
		String username, password;
		int choix,id;
		System.out.println("******************************************************************************************");
		System.out.println("****************** Bienvenu Professeur *******************\n");
		
		System.out.println("-------------- Veuillez vous authentifier pour continuer -------\n");
		System.out.println("Renseigner votre identifiant svp (Nombre entier) : ");
		id = scan.nextInt();
		System.out.println("Renseigner votre nom d'utilisateur svp:");
		username = scan.nextLine();
		
		System.out.println("Indiquer votre mot de passe svp: ");
		password = scan.nextLine();
		
		Enseignant ens = enser.verifUser(id);
		
		if(ens.getUsername().equals(username) && ens.getPassword().equals(password)) {
			
			System.out.println("Connected ::: [Profil] ::: " +ens.getUsername());

		System.out.println("************* MENU ************\n");
		System.out.println("1- Enregistrer Notes \n 2- Afficher les Notes attribuer\n 3- Consulter son inbox\n 4- Action sur une reclamation");
		choix = scan.nextInt();
		switch(choix) {
		case 1:
			int reponse,id1;
			String valeur;
			float note;
			EtudiantService eser = new EtudiantService();
			UeService usr = new UeService();
			NotesService ns = new NotesService();
			Notes N = new Notes();
			eser.getIt();
			do {
				System.out.println("Veuillez saisir l'identifiant de l'etudiant : ");
				id1 = scan.nextInt();
				Etudiant etudiant = eser.getById(id1);
				
				System.out.println(etudiant);
				
				N.setEtudiants(etudiant);
				
				System.out.println("specifier l'unite d'enseignement de preference le libelle de l'UE : ");
				
				valeur =scan.nextLine();
				valeur = scan.nextLine();
				Ue ue = usr.getByNom(valeur);
				N.setUes(ue);
				
				System.out.println("Attribuer une note : ");
				note = scan.nextFloat();
				N.setNote(note);
				
				ns.Addnote(N);
				
				System.out.println("Voulez vous ajouter une note? :");
				reponse = scan.nextInt();
				
			}while(reponse == 0);
				
			break;
		case 2:
			int identifiant;
			String codeUE;
			System.out.println("******************  Pages des notes etudiants *******************");
			NotesService Noteserv = new NotesService();
			System.out.println("Renseigner identifiant de l'etudiant concerner: \n");
			identifiant = scan.nextInt();
			System.out.println("Le code de votre unité d'enseignement: \n");
			codeUE = scan.nextLine();
			codeUE = scan.nextLine();
			Noteserv.getByCodeUE(identifiant, codeUE);
			break;
		case 3:
			System.out.println("Bienvenu Monsieur :::" +ens.getUsername());
			System.out.println("+++++++++++++++++ Page de lecture de message ++++++++++++++++\n");
			enser.consulterInbox(id);
			break;
		case 4:
			break;
		default:
			System.out.println("Impossible!!!!!");
		}
	}else {
		System.out.println("Athentification failed");
	}
	}
}